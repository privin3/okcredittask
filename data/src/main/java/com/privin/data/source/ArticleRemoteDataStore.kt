package com.privin.data.source

import com.privin.data.entity.ArticleEntity
import com.privin.data.repository.ArticleDataStore
import com.privin.data.repository.ArticleRemote
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.lang.UnsupportedOperationException
import javax.inject.Inject

open class ArticleRemoteDataStore @Inject constructor(private val articleRemote: ArticleRemote) : ArticleDataStore {
    override fun clearArticles(section: String): Completable {
        throw UnsupportedOperationException()
    }

    override fun saveArticles(articles: List<ArticleEntity>,section: String): Completable {
        throw UnsupportedOperationException()
    }

    override fun getArticles(section :String): Flowable<List<ArticleEntity>> {
        println("memory getArticles remote $section")
        return articleRemote.getArticles(section)
    }

    override fun isCached(section: String): Single<Boolean> {
        throw UnsupportedOperationException()
    }

}