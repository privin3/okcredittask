package com.privin.data.source

import com.privin.data.repository.ArticleCache
import com.privin.data.repository.ArticleDataStore
import javax.inject.Inject

class ArticleDataStoreFactory @Inject constructor(
    private val articleCache: ArticleCache,
    private val articleCacheDataStore: ArticleCacheDataStore,
    private val articleRemoteDataStore: ArticleRemoteDataStore){

    fun getRemoteDataStore() : ArticleDataStore{
        return articleRemoteDataStore
    }
    fun getCacheDataStore() : ArticleDataStore {
        return articleCacheDataStore
    }
    fun getDataStore(isCached : Boolean) : ArticleDataStore{
        if (isCached && !articleCache.isExpired()){
            return articleCacheDataStore
        }
        return articleRemoteDataStore
    }
}