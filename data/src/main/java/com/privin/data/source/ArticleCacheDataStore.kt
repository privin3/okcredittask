package com.privin.data.source

import com.privin.data.entity.ArticleEntity
import com.privin.data.repository.ArticleCache
import com.privin.data.repository.ArticleDataStore
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.lang.UnsupportedOperationException
import javax.inject.Inject

class ArticleCacheDataStore @Inject constructor(private val articleCache: ArticleCache) : ArticleDataStore {
    override fun clearArticles(section: String): Completable {
        return articleCache.clearArticles(section)
    }

    override fun saveArticles(articles: List<ArticleEntity>,section: String): Completable {
        return articleCache.saveArticles(articles,section)
            .doOnComplete {
                println(" memory saveArticles "+ System.currentTimeMillis())
                articleCache.setLastCacheTime(System.currentTimeMillis()) }
    }

    override fun getArticles(section :String): Flowable<List<ArticleEntity>> {
        println(" memory getArticles before "+ System.currentTimeMillis()+" "+ Thread.currentThread().name)
        return articleCache.getArticles(section).doOnComplete {
            println(" memory getArticles after "+ System.currentTimeMillis()+" "+ Thread.currentThread().name)
        }
    }

    override fun isCached(section: String): Single<Boolean> {
        return articleCache.isCached(section)
    }
}