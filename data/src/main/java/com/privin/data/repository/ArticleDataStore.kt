package com.privin.data.repository

import com.privin.data.entity.ArticleEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface ArticleDataStore {

    fun clearArticles(section: String): Completable

    fun saveArticles(articles: List<ArticleEntity>,section: String): Completable

    fun getArticles(section :String): Flowable<List<ArticleEntity>>

    fun isCached(section: String): Single<Boolean>
}