package com.privin.data.repository

import com.privin.data.entity.ArticleEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface ArticleCache {


    fun clearArticles(section: String): Completable

    fun saveArticles(articles: List<ArticleEntity>,section : String): Completable

    fun getArticles(section: String): Flowable<List<ArticleEntity>>

    /**
     * Check whether there is a list of Bufferoos stored in the cache.
     *
     * @return true if the list is cached, otherwise false
     */
    fun isCached(section: String): Single<Boolean>

    /**
     * Set a point in time at when the cache was last updated.
     *
     * @param lastCache the point in time at when the cache was last updated
     */
    fun setLastCacheTime(lastCache: Long)

    /**
     * Check if the cache is expired.
     *
     * @return true if the cache is expired, otherwise false
     */
    fun isExpired(): Boolean
}