package com.privin.data.repository

import com.privin.data.entity.ArticleEntity
import io.reactivex.Flowable

interface ArticleRemote{

    fun getArticles(section : String) : Flowable<List<ArticleEntity>>
}