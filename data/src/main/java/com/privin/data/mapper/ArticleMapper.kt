package com.privin.data.mapper

import com.privin.data.entity.ArticleEntity
import com.privin.data.entity.MultiMediaEntity
import com.privin.domain.model.Article
import com.privin.domain.model.MultiMedia
import java.lang.UnsupportedOperationException
import javax.inject.Inject

class ArticleMapper @Inject constructor() : Mapper<ArticleEntity,Article> {

    override fun mapFromEntity(type: ArticleEntity): Article {
        val list= ArrayList<MultiMedia>()
        type.multiMedia?.forEach {

            val multiMedia = MultiMedia(url = it.url,
                format = it.format,
                height = it.height,
                width = it.width,
                type = it.type,
                subType = it.subType,
                caption = it.caption,
                copyright = it.copyright)
            list.add(multiMedia)
        }
        return Article(title = type.title,
            author = type.author,
            section = type.section,
            abstract = type.abstract,
            type = type.type,
            url = type.url,
            publishedDate = type.publishedDate,
            multiMedia = list)
    }

    override fun mapToEntity(type: Article): ArticleEntity {
        val multiMediaList = mutableListOf<MultiMediaEntity>()
        type.multiMedia?.forEach { multiMediaList.add(MultiMediaEntity(url = it.url,
            format = it.format,
            height = it.height,
            width = it.width,
            type = it.type,
            subType = it.subType,
            caption = it.caption,
            copyright = it.copyright)) }

        return ArticleEntity(title = type.title,
            author = type.author,
            section = type.section,
            abstract = type.abstract,
            type = type.type,
            url = type.url,
            publishedDate = type.publishedDate,
            multiMedia = multiMediaList)
    }
}