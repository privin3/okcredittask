package com.privin.data.entity

data class MultiMediaEntity(val url : String?,
                            val format: String?,
                            val height : Int?,
                            val width: Int?,
                            val type : String?,
                            val subType : String?,
                            val caption : String?,
                            val copyright : String?) {
}