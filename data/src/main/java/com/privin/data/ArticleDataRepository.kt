package com.privin.data

import com.privin.data.entity.ArticleEntity
import com.privin.data.mapper.ArticleMapper
import com.privin.data.mapper.Mapper
import com.privin.data.repository.ArticleDataStore
import com.privin.data.source.ArticleDataStoreFactory
import com.privin.domain.ArticleRepository
import com.privin.domain.model.Article
import io.reactivex.Completable
import io.reactivex.Flowable
import java.lang.UnsupportedOperationException
import javax.inject.Inject

class ArticleDataRepository @Inject constructor(
    private val articleDataStoreFactory: ArticleDataStoreFactory,
    private val mapper: ArticleMapper) : ArticleRepository{
    override fun clearArticles(section: String): Completable {
        return articleDataStoreFactory.getCacheDataStore().clearArticles(section)
    }

    override fun saveArticle(articles: List<Article>,section: String): Completable {
        val articleList = mutableListOf<ArticleEntity>()
        articles.map { articleList.add(mapper.mapToEntity(it)) }
        return articleDataStoreFactory.getCacheDataStore().saveArticles(articleList,section)
    }

    override fun getArticles(section :String,fromRemote : Boolean): Flowable<List<Article>> {
        println("memory getArticles adr $section $fromRemote")
        if(fromRemote){
            return articleDataStoreFactory.getRemoteDataStore().getArticles(section)
                .flatMap { article -> Flowable.just(article.map { mapper.mapFromEntity(it) }) }
                .flatMap {
                    clearArticles(section)
                    saveArticle(it,section).toSingle{ it }.toFlowable() }
        }
        return articleDataStoreFactory.getCacheDataStore().isCached(section)
            .flatMapPublisher { articleDataStoreFactory.getDataStore(it).getArticles(section) }
            .flatMap { article -> Flowable.just(article.map { mapper.mapFromEntity(it) }) }
    }
}