package com.privin.presenter

import com.privin.presenter.base.BaseIntent

sealed class SharedIntent(section :String? = null,fromRemote: Boolean = false) : BaseIntent {
    data class LoadHomeIntent(val section :String,val fromRemote: Boolean = false) : SharedIntent(section,fromRemote)
    object InitialHomeIntent : SharedIntent()

    object InitialDetailIntent : SharedIntent()
}