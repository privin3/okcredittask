package com.privin.presenter

import com.privin.presenter.base.BaseState
import com.privin.presenter.model.ArticleView

sealed class SharedState(val inProgress : Boolean = false,
                         val articles : List<ArticleView>? = null,
                         val section : String = "",
                         val msg : String? = null) : BaseState {
    object InProgress : SharedState(true)
    data class Failed(val m :String?) : SharedState(msg = m)
    data class Success(var result: List<ArticleView>?,var sec: String) : SharedState(false,result,sec)
    class Idle : SharedState()
}