package com.privin.presenter

import com.privin.domain.model.Article
import com.privin.presenter.base.BaseResult

sealed class SharedResult: BaseResult {
    class LoadHomeTask(val task : TaskStatus,
                       val section : String = "",
                       val msg : String?= "",
                       val articles : List<Article>? = null) : SharedResult(){
       companion object{
           internal fun success(list : List<Article>?,sec: String): LoadHomeTask{
               return LoadHomeTask(TaskStatus.SUCCESS,sec,articles = list)
           }
           internal fun failed(msg : String?): LoadHomeTask{
               return LoadHomeTask(TaskStatus.FAIL,msg = msg)
           }
           internal fun inProgress(): LoadHomeTask{
               return LoadHomeTask(TaskStatus.IN_PROGRESS)
           }
       }
    }
}