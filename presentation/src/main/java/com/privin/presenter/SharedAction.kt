package com.privin.presenter

import com.privin.presenter.base.BaseAction

sealed class SharedAction(section :String? = null,fromRemote: Boolean? = false) : BaseAction{
    data class LoadArticles(val section :String,val fromRemote : Boolean) : SharedAction(section,fromRemote)
    object Idle : SharedAction()
}