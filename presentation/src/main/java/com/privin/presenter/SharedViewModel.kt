package com.privin.presenter

import androidx.lifecycle.ViewModel
import com.privin.presenter.base.BaseIntent
import com.privin.presenter.base.BaseViewModel
import com.privin.presenter.base.BaseState
import com.privin.presenter.mapper.ArticleMapper
import com.privin.presenter.model.ArticleView
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import java.lang.UnsupportedOperationException
import javax.inject.Inject

class SharedViewModel @Inject internal constructor(private val actionProcessor: SharedActionProcessor,
                                                   private val mapper: ArticleMapper) : ViewModel(),BaseViewModel<SharedIntent,SharedState> {
    private var intentSubject : PublishSubject<SharedIntent> = PublishSubject.create()

    var ramCache = HashMap<String,List<ArticleView>>()

    private var intentFilter : ObservableTransformer<SharedIntent, SharedIntent> =
        ObservableTransformer {
            it.publish { Observable.merge(it.ofType(SharedIntent.InitialHomeIntent::class.java).take(1),
                it.filter{intent -> intent !is SharedIntent.InitialHomeIntent}) }
        }

    private val reducer : BiFunction<SharedState, SharedResult, SharedState> =
                BiFunction {prevState,result ->
                    when(result){
                        is SharedResult.LoadHomeTask ->
                            when(result.task){
                                TaskStatus.SUCCESS -> SharedState.Success(result.articles?.map {article-> mapper.mapToView(article) },result.section)
                                TaskStatus.FAIL -> SharedState.Failed(result.msg)
                                TaskStatus.IN_PROGRESS -> SharedState.InProgress
                            }
                    }
                }
    private var states = compose()

    private fun compose(): Observable<SharedState> {
        println("in compose")
        return intentSubject.compose(intentFilter)
            .map { this.actionFromIntent(it) }
            .compose(actionProcessor.homeActionProcessor)
            .scan<SharedState>(SharedState.Idle(),reducer)
            .replay(1)
            .autoConnect()
    }

    private fun actionFromIntent(intent: SharedIntent) : SharedAction{
        return when(intent){
            is SharedIntent.InitialHomeIntent -> SharedAction.Idle
            is SharedIntent.LoadHomeIntent -> SharedAction.LoadArticles(intent.section,intent.fromRemote)
            is SharedIntent.InitialDetailIntent -> SharedAction.Idle
            else -> throw UnsupportedOperationException("unknown intent : $intent")
        }
    }

    override fun processIntents(intents: Observable<SharedIntent>) {
        intents.subscribe(intentSubject)
    }

    override fun states(): Observable<SharedState> {
        return states
    }
}