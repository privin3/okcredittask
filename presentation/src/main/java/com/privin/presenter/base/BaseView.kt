package com.privin.presenter.base

import io.reactivex.Observable


interface BaseView<I : BaseIntent,in S : BaseState>{
    fun intents() : Observable<I>
    fun render(state : S)
}