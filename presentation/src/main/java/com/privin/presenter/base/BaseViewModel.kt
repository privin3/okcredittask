package com.privin.presenter.base

import io.reactivex.Observable


interface BaseViewModel<I : BaseIntent, S : BaseState> {
    fun processIntents(intents : Observable<I>)
    fun states() : Observable<S>
}