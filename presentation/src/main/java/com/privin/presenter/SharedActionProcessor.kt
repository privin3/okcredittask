package com.privin.presenter

import com.privin.domain.interactor.articles.GetArticlesUseCase
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import java.lang.IllegalArgumentException
import javax.inject.Inject

class SharedActionProcessor @Inject constructor(private val getArticlesUseCase: GetArticlesUseCase) {
    private val homeConversionProcessor : ObservableTransformer<SharedAction.LoadArticles, SharedResult> =
        ObservableTransformer { it ->
            it.switchMap {
                getArticlesUseCase.execute(GetArticlesUseCase.Data(it.section,it.fromRemote))
                    .map { list -> SharedResult.LoadHomeTask.success(list,it.section) }
                    .onErrorReturn { SharedResult.LoadHomeTask.failed(it.message) }
                    .toObservable()
                    .startWith(SharedResult.LoadHomeTask.inProgress())
            }
        }

    var homeActionProcessor : ObservableTransformer<SharedAction,SharedResult>

    init {
        homeActionProcessor = ObservableTransformer {
            it.publish{ it ->
                it.ofType(SharedAction.LoadArticles::class.java)
                    .compose(homeConversionProcessor)
                    .mergeWith(it.filter{ it !is SharedAction.LoadArticles}
                        .flatMap { Observable.error<SharedResult>( IllegalArgumentException("Unknown Action Type + ${it}")) })

            }
        }
    }



}