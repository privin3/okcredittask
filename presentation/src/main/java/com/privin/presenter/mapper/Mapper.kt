package com.privin.presenter.mapper

interface Mapper<out O,in I> {
    fun mapToView(type : I) : O
}