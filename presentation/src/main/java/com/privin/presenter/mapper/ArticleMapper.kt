package com.privin.presenter.mapper

import com.privin.domain.model.Article
import com.privin.presenter.model.ArticleView
import javax.inject.Inject

open class ArticleMapper @Inject constructor() :
    Mapper<ArticleView, Article> {
    override fun mapToView(article: Article): ArticleView {
        var thumbNail : String? = null
        var largeImage : String? = null
        article.multiMedia?.map {
            when(it.format){
                "mediumThreeByTwo210" -> largeImage = it.url
                "Standard Thumbnail" -> thumbNail = it.url
            }
        }
        return ArticleView(title = article.title,
            authorName = article.author,
            thumbnailImage = thumbNail,
            largeImage = largeImage,
            publishedOn = article.publishedDate,
            sourceUrl = article.url,
            description = article.abstract)
    }

}