package com.privin.presenter.model

import android.os.Parcel
import android.os.Parcelable

data class ArticleView (val title :String?,
                        val authorName : String?,
                        val thumbnailImage : String?,
                        val largeImage : String?,
                        val publishedOn : String?,
                        val sourceUrl : String?,
                        val description : String?) : Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(authorName)
        parcel.writeString(thumbnailImage)
        parcel.writeString(largeImage)
        parcel.writeString(publishedOn)
        parcel.writeString(sourceUrl)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArticleView> {
        override fun createFromParcel(parcel: Parcel): ArticleView {
            return ArticleView(parcel)
        }

        override fun newArray(size: Int): Array<ArticleView?> {
            return arrayOfNulls(size)
        }
    }
}