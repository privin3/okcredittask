package com.privin.remote

import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit

object ArticleServiceFactory {
    lateinit var API_KEY: String

    fun createService(baseUrl : String,apiKey : String,isDebug : Boolean) : ArticleService{
        API_KEY = apiKey
        val okHttpClient = makeOkHttpClient(makeHttpLoggingInterceptor(isDebug))
        return makeRetrofit(baseUrl,okHttpClient,Gson())
    }

    private fun makeRetrofit(baseUrl: String,okHttpClient: OkHttpClient,gson: Gson) : ArticleService{
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        return retrofit.create(ArticleService::class.java)
    }

    private fun makeOkHttpClient(interceptor: HttpLoggingInterceptor) : OkHttpClient{
        return OkHttpClient.Builder().addInterceptor(interceptor)
            .connectTimeout(120,TimeUnit.SECONDS)
            .readTimeout(120,TimeUnit.SECONDS)
            .build()
    }

    private fun makeHttpLoggingInterceptor(isDebug: Boolean) : HttpLoggingInterceptor{
        val logging = HttpLoggingInterceptor()
        logging.level = if (isDebug) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return logging
    }

}