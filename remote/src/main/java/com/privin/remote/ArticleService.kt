package com.privin.remote

import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ArticleService {

    @GET("/svc/topstories/v2/{section}.json")
    fun getArticles(@Path("section") section : String,
                    @Query("api-key") apiKey : String = ArticleServiceFactory.API_KEY) : Flowable<ArticleResponse>
}