package com.privin.remote

import com.privin.remote.model.ArticleModel

class ArticleResponse(val status : String?,
                      val copyright : String?,
                      val section : String?,
                      val last_updated : String?,
                      val results : List<ArticleModel>?) {

}