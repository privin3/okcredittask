package com.privin.remote

import com.privin.data.entity.ArticleEntity
import com.privin.data.repository.ArticleRemote
import com.privin.remote.mapper.ArticleEntityMapper
import com.privin.remote.model.ArticleModel
import com.privin.remote.model.MultiMediaModel
import io.reactivex.Flowable
import javax.inject.Inject

class RemoteData @Inject constructor(private val articleService: ArticleService,
                                     private val articleEntityMapper: ArticleEntityMapper) : ArticleRemote{


    override fun getArticles(section : String): Flowable<List<ArticleEntity>> {
        println("getArticles $section")
        return articleService.getArticles(section)
            .flatMap { it ->
                println("in remoteData : ${it.status}")
                println("in remoteData : ${it.results?.size}")
                Flowable.just(it.results?.map { articleEntityMapper.mapToEntity(it) }) }
    }
}