package com.privin.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ArticleModel(
    @Expose
    @SerializedName("title")
    val title : String,
    @Expose
    @SerializedName("byline")
    val author : String,
    @Expose
    @SerializedName("section")
    val section : String?,
    @Expose
    @SerializedName("abstract")
    val abstract : String,
    @Expose
    @SerializedName("item_type")
    val type : String?,
    @Expose
    @SerializedName("short_url")
    val url : String?,
    @Expose
    @SerializedName("published_date")
    val publishedDate : String?,
    @Expose
    @SerializedName("multimedia")
    val multiMedia : List<MultiMediaModel>?) {
}