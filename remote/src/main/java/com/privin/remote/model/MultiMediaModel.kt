package com.privin.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MultiMediaModel(
    @Expose
    @SerializedName("url")
    val url : String?,
    @Expose
    @SerializedName("format")
    val format: String?,
    @Expose
    @SerializedName("height")
    val height : Int?,
    @Expose
    @SerializedName("width")
    val width: Int?,
    @Expose
    @SerializedName("type")
    val type : String?,
    @Expose
    @SerializedName("subType")
    val subType : String?,
    @Expose
    @SerializedName("caption")
    val caption : String?,
    @Expose
    @SerializedName("copyright")
    val copyright : String?) {
}