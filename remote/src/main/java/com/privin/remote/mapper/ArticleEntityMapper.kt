package com.privin.remote.mapper

import com.privin.data.entity.ArticleEntity
import com.privin.data.entity.MultiMediaEntity
import com.privin.remote.model.ArticleModel
import com.privin.remote.model.MultiMediaModel
import javax.inject.Inject

open class ArticleEntityMapper @Inject constructor(): Mapper<ArticleModel,ArticleEntity>{

    override fun mapToEntity(model: ArticleModel): ArticleEntity {
        val list= ArrayList<MultiMediaEntity>()
        model.multiMedia?.forEach {

            val multiMedia = MultiMediaEntity(url = it.url,
                format = it.format,
                height = it.height,
                width = it.width,
                type = it.type,
                subType = it.subType,
                caption = it.caption,
                copyright = it.copyright)
            list.add(multiMedia)
        }
        return ArticleEntity(title = model.title,
            author = model.author,
            section = model.section,
            abstract = model.abstract,
            type = model.type,
            url = model.url,
            publishedDate = model.publishedDate,
            multiMedia = list)
    }
}