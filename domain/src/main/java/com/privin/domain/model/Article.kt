package com.privin.domain.model

data class Article(val title : String?,
                   val author : String?,
                   val section : String?,
                   val abstract : String?,
                   val type : String?,
                   val url : String?,
                   val publishedDate : String?,
                   val multiMedia : List<MultiMedia>?) {
}