package com.privin.domain

import com.privin.domain.model.Article
import io.reactivex.Completable
import io.reactivex.Flowable

interface ArticleRepository {

    fun clearArticles(section: String) : Completable

    fun saveArticle(articles : List<Article>,section: String) : Completable

    fun getArticles(section :String,fromRemote : Boolean) : Flowable<List<Article>>

}