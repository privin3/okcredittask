package com.privin.domain.interactor

import com.privin.domain.executor.PostExecutionThread
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers

abstract class FlowableUsecase<T,in Params> constructor(private val postExecutionThread: PostExecutionThread) {


    abstract fun buildObservable(params: Params? = null) : Flowable<T>

    fun execute(params: Params? = null) : Flowable<T>{
            return buildObservable(params)
                .subscribeOn(Schedulers.io())
                .observeOn(postExecutionThread.scheduler)
    }

}