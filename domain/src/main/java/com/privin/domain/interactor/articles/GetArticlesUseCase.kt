package com.privin.domain.interactor.articles

import com.privin.domain.ArticleRepository
import com.privin.domain.executor.PostExecutionThread
import com.privin.domain.interactor.FlowableUsecase
import com.privin.domain.model.Article
import io.reactivex.Flowable
import java.lang.IllegalArgumentException
import javax.inject.Inject

open class GetArticlesUseCase @Inject constructor(private val articleRepository: ArticleRepository,
                                          postExecutionThread: PostExecutionThread): FlowableUsecase<List<Article>, GetArticlesUseCase.Data?>(postExecutionThread){

    class Data(val section: String?,
               val fromRemote : Boolean = false)
    override fun buildObservable(params: Data?): Flowable<List<Article>> {
        println("usecase section $params")
        if (params==null || params.section==null)
            throw IllegalArgumentException()
        return articleRepository.getArticles(params.section,params.fromRemote)
    }
}