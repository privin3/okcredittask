package com.privin.okcredittask.util

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class HelperTest{

    @Test
    fun checkDateFormat(){
        assertEquals("Feb. 25 2020, 03:02 PM",Helper.getFormattedDate("2020-02-25T04:32:24-05:00"))
    }
}