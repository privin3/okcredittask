package com.privin.okcredittask

import android.app.Activity
import android.app.Application
import com.privin.okcredittask.di.AppInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import java.util.*
import javax.inject.Inject


class App : Application(),HasAndroidInjector {
    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)
    }


    override fun androidInjector(): AndroidInjector<Any>? {
        return activityDispatchingAndroidInjector
    }
}