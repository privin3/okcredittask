package com.privin.okcredittask.view.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.privin.okcredittask.R
import com.privin.presenter.model.ArticleView
import kotlinx.android.synthetic.main.adapter_article_item.view.*
import javax.inject.Inject

class HomeAdapter @Inject constructor(val delegate: HomeAdapterDelegate): ListAdapter<ArticleView, HomeAdapter.ArticleViewHolder>(
    diffCallback) {

    companion object {

        private val diffCallback = object : DiffUtil.ItemCallback<ArticleView>() {
            override fun areItemsTheSame(oldItem: ArticleView, newItem: ArticleView): Boolean =
                oldItem.title == newItem.title

            override fun areContentsTheSame(oldItem: ArticleView, newItem: ArticleView): Boolean =
                oldItem == newItem
        }
    }

    interface HomeAdapterDelegate{
        fun onItemClicked(articleView: ArticleView)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.adapter_article_item,parent,false)
        return ArticleViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        val article = getItem(position)
        holder.title.text = article.title
        holder.author.text = if(article.authorName!=null && article.authorName!!.isNotEmpty()) article.authorName else "Unknown"
        holder.loadImage(article.thumbnailImage)
    }

    inner class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val title:TextView =  itemView.title
        val author:TextView = itemView.author
        private val thumbNail:ImageView = itemView.image
        private val parentView : LinearLayout = itemView.article_item

        init {
            setListeners()
        }

        private fun setListeners() {
            parentView.setOnClickListener{
                delegate.onItemClicked(getItem(adapterPosition))
            }
        }

        fun loadImage(url : String?){
            Glide
                .with(itemView)
                .load(url)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(thumbNail)
        }
    }
}