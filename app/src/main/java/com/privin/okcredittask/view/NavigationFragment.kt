package com.privin.okcredittask.view


import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout

import com.privin.okcredittask.R
import com.privin.okcredittask.util.setToolbarUp
import java.lang.IllegalArgumentException
import javax.annotation.Resource

/**
 * A simple [Fragment] subclass.
 */
class NavigationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation, container, false)
    }

    private val sectionList = arrayListOf("Science","Technology","Business","World","Movies")
    lateinit var viewPager : ViewPager
    private lateinit var tab : TabLayout
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPager = view.findViewById(R.id.view_pager)
        tab = view.findViewById(R.id.tabs)
        setupViewPager()
        tab.setupWithViewPager(viewPager)
        setTabIcons()
    }

    private fun setTabIcons(){
        tab.getTabAt(0)?.icon = context?.resources?.getDrawable(R.drawable.ic_science,resources.newTheme())
        tab.getTabAt(1)?.icon = context?.resources?.getDrawable(R.drawable.ic_tech,resources.newTheme())
        tab.getTabAt(2)?.icon = context?.resources?.getDrawable(R.drawable.ic_business,resources.newTheme())
        tab.getTabAt(3)?.icon = context?.resources?.getDrawable(R.drawable.ic_world,resources.newTheme())
        tab.getTabAt(4)?.icon = context?.resources?.getDrawable(R.drawable.ic_movie,resources.newTheme())
    }

    private fun setupViewPager() {
        fragmentManager?.let{
            val adapter = ViewPagerAdapter(it,sectionList)
            viewPager.adapter = adapter
        }
    }

}

