package com.privin.okcredittask.view.home


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.privin.okcredittask.R
import com.privin.okcredittask.di.injectable
import com.privin.okcredittask.util.call
import com.privin.okcredittask.view.MainActivity
import com.privin.okcredittask.view.detail.DetailFragment
import com.privin.presenter.SharedIntent
import com.privin.presenter.SharedState
import com.privin.presenter.SharedViewModel
import com.privin.presenter.base.BaseView
import com.privin.presenter.model.ArticleView
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment(),BaseView<SharedIntent,SharedState>,injectable{

    private val loadIntent  = BehaviorSubject.create<SharedIntent.LoadHomeIntent>()
    private val initialIntent  = BehaviorSubject.create<SharedIntent.InitialHomeIntent>()
    lateinit var viewModel : SharedViewModel
    private val compositeDisposable = CompositeDisposable()
    lateinit var adapter: HomeAdapter
    lateinit var section: String



    companion object{

        fun newInstance(section : String) : HomeFragment{
            val args = Bundle()
            args.putString("section",section)
            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            section = it.getString("section","")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        viewModel = (activity as MainActivity).viewModel
        compositeDisposable.add(viewModel.states().subscribe { render(it) })
        viewModel.processIntents(intents())
        loadData()
        swipe_refresh.setColorSchemeResources(R.color.colorAccent)
        swipe_refresh.setOnRefreshListener {
            println("section $section")
            loadIntent.onNext(SharedIntent.LoadHomeIntent(section,true))
        }
    }

    private fun loadData(){
        viewModel.ramCache[section]?.let {
            if (it.isNotEmpty()) {
                println("memory onResume if $section")
                renderList(it, section)
                return
            }
        }
        prepareData()
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }
    private fun prepareData(){
        println("memory preparing data")
        loadIntent.onNext(SharedIntent.LoadHomeIntent(section))
    }

    private fun setupRecyclerView() {
        adapter = HomeAdapter(AdapterListener())
        recycler_view.adapter = adapter
        recycler_view.layoutManager = LinearLayoutManager(context)
    }


    override fun intents(): Observable<SharedIntent> {
        return Observable.merge(initialIntent,loadIntent)
    }


    override fun render(state: SharedState) {
        when(state){
            SharedState.InProgress -> renderLoadingState()
            is SharedState.Failed -> renderFailedState(state.msg)
            is SharedState.Success ->{
                saveData(state.result,state.sec)
                renderList(state.result,state.sec)
            }
            is SharedState.Idle -> println("State Idle")
        }
    }

    private fun saveData(result: List<ArticleView>?, sec: String) {
        result?.let {
            if (it.isNotEmpty()) {
                viewModel.ramCache[sec] = it
                println("memory saveData $section")
            }
        }
    }

    private fun renderLoadingState() {
        println("State in progress")
    }
    private fun renderFailedState(msg :String?) {
        swipe_refresh.isRefreshing = false
        Toast.makeText(context,"Failed $msg",Toast.LENGTH_SHORT).show()
        println("failed :  $msg")
    }

    private fun renderList(list : List<ArticleView>?,sec: String){
        swipe_refresh.isRefreshing = false
        if (list==null){
            Toast.makeText(context,"List is Empty",Toast.LENGTH_SHORT).show()
            return
        }

        if (isResumed){
            println("memory renderList $section")
            adapter.submitList(list)
        }
    }


    inner class AdapterListener : HomeAdapter.HomeAdapterDelegate{
        override fun onItemClicked(articleView: ArticleView) {
            val detailFragment = DetailFragment.newInstance(articleView)
            call(detailFragment)
        }

    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}
