package com.privin.okcredittask.view

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import com.privin.okcredittask.view.home.HomeFragment

class ViewPagerAdapter(fm : FragmentManager, private val sections : ArrayList<String>) : FragmentStatePagerAdapter(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {


    override fun getItem(position: Int): Fragment {
        return HomeFragment.newInstance(sections[position])
    }

    override fun getCount(): Int {
        return sections.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return sections[position]
    }
}