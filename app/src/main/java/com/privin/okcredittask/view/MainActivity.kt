package com.privin.okcredittask.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.privin.okcredittask.R
import com.privin.okcredittask.view.home.HomeFragment
import com.privin.presenter.SharedViewModel
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(),HasAndroidInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    @Inject lateinit var viewModel : SharedViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = ""
        if(supportFragmentManager.fragments.size==0){
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.container,NavigationFragment(),"home")
            ft.commit()
        }

    }

    override fun androidInjector(): AndroidInjector<Any> {
        return fragmentDispatchingAndroidInjector
    }

    private fun popFragment() : Boolean{
        if(supportFragmentManager.findFragmentByTag("detail") !=null){
            supportFragmentManager.popBackStack()
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            return true
        }
        return false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            return popFragment()
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onBackPressed() {
        if (popFragment())
            return
        super.onBackPressed()
    }

}
