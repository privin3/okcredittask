package com.privin.okcredittask.view.detail


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide

import com.privin.okcredittask.R
import com.privin.okcredittask.util.Helper
import com.privin.okcredittask.util.setToolbarUp
import com.privin.okcredittask.view.home.HomeFragment
import com.privin.presenter.model.ArticleView
import kotlinx.android.synthetic.main.adapter_article_item.*
import kotlinx.android.synthetic.main.fragment_detail.*

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {

    companion object{

        fun newInstance(article : ArticleView) : DetailFragment{
            val args = Bundle()
            args.putParcelable("article",article)
            val fragment = DetailFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var article: ArticleView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        arguments?.let {
            article = it.getParcelable<ArticleView>("article") as ArticleView
        }

        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        article_title.text = article.title
        published.text = Helper.getFormattedDate(article.publishedOn)
        description.text = article.description
        author_name.text = if(article.authorName!=null && article.authorName!!.isNotEmpty()) article.authorName else "Unknown"
        source.text = article.sourceUrl
        loadImage(article.largeImage)
    }

    private fun loadImage(url: String?) {
        Glide
            .with(this)
            .load(url)
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(large_image)
    }

    override fun onResume() {
        super.onResume()
        setToolbarUp(true)
    }

}
