package com.privin.okcredittask.util

import java.text.SimpleDateFormat
import java.util.*

class Helper {
    companion object{
        fun getFormattedDate(date :String?): String{
            val dataFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssXXX", Locale.getDefault())
            val displayFormat = SimpleDateFormat("MMM. dd yyyy, hh:mm a", Locale.getDefault())
            date?.let {selfDate->
                val tempDate = dataFormat.parse(selfDate)
                tempDate?.let {
                    return displayFormat.format(it)
                }
            }
            return ""
        }
    }
}