package com.privin.okcredittask.util

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.privin.okcredittask.R

fun Fragment.call(fragment: Fragment) {
    activity?.supportFragmentManager?.beginTransaction()
        ?.add(R.id.container,fragment,"detail")
        ?.addToBackStack("home")
        ?.commit()
}

fun Fragment.setToolbarUp(bool: Boolean){
    val parent = activity as AppCompatActivity
    parent.supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
    parent.supportActionBar?.setDisplayHomeAsUpEnabled(bool)
}
