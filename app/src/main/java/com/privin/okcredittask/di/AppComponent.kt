package com.privin.okcredittask.di

import android.app.Application
import com.privin.okcredittask.App
import com.privin.okcredittask.di.modules.*
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    PresentationModule::class,
    RemoteModule::class,
    LocalModule::class,
    DomainModule::class,
    DataModule::class,
    UiModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application) : Builder
        fun build() : AppComponent
    }

    fun inject(app : App)
}