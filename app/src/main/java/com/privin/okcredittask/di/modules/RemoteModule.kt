package com.privin.okcredittask.di.modules

import com.privin.data.repository.ArticleRemote
import com.privin.okcredittask.BuildConfig
import com.privin.remote.ArticleService
import com.privin.remote.ArticleServiceFactory
import com.privin.remote.RemoteData
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class RemoteModule {

    @Module
    companion object{
        @Provides
        @JvmStatic
        fun provideArticleService() : ArticleService{
            return ArticleServiceFactory.createService(BuildConfig.BASE_URL,BuildConfig.API_KEY,BuildConfig.DEBUG)
        }
    }
    @Binds
    abstract fun bindsRemoteData(remoteData: RemoteData) : ArticleRemote
}