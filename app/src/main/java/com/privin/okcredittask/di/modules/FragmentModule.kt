package com.privin.okcredittask.di.modules

import com.privin.okcredittask.view.home.HomeAdapter
import com.privin.okcredittask.view.home.HomeFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

}