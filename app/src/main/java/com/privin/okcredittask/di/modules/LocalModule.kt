package com.privin.okcredittask.di.modules

import android.app.Application
import androidx.room.Room
import com.privin.data.repository.ArticleCache
import com.privin.local.ArticleLocalRepo
import com.privin.local.database.ArticleDatabase
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides

@Module
abstract class LocalModule {

    @Module
    companion object{

        @Provides
        @JvmStatic
        fun providesDatabase(application :Application) : ArticleDatabase{
            return ArticleDatabase.getInstance(application.applicationContext)
        }
    }

    @Binds
    abstract fun bindsArticleCache(articleLocalRepo: ArticleLocalRepo) : ArticleCache
}