package com.privin.okcredittask.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException
import java.lang.RuntimeException
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class ViewModelFactory @Inject constructor(
    private val creators: Map<Class<out ViewModel>,
            @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        var creator : Provider<out ViewModel>? = creators[modelClass]
        if (creator==null){
            for ((k,v) in creators){
                if (modelClass.isAssignableFrom(k)){
                    creator = v
                    break
                }
            }
        }
        if (creator==null){
            throw IllegalArgumentException("unknown modelclass $modelClass")
        }
        try {
            return creator.get() as T
        }catch (e : Exception){
            throw RuntimeException()
        }
    }
}