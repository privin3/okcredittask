package com.privin.okcredittask.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.privin.okcredittask.App
import dagger.android.AndroidInjection
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection

class AppInjector {
    companion object{
        fun init(app: App){
            DaggerAppComponent.builder()
                .application(app)
                .build()
                .inject(app)
            app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks{
                override fun onActivityCreated(activity : Activity, savedStateInstance : Bundle?) {
                    println("App injector : onActivityCreated")
                    handleActivity(activity)
                }

                override fun onActivityPaused(p0: Activity) {
                    println("App injector : onActivityPaused")
                }

                override fun onActivityStarted(p0: Activity) {
                    println("App injector : onActivityStarted")
                }

                override fun onActivityDestroyed(p0: Activity) {
                    println("App injector : onActivityDestroyed")
                }

                override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
                    println("App injector : onActivitySaveInstanceState")
                }

                override fun onActivityStopped(p0: Activity) {
                    println("App injector : onActivityStopped")
                }


                override fun onActivityResumed(p0: Activity) {
                    println("App injector : onActivityResumed")
                }
            })
        }

        private fun handleActivity(activity : Activity){
            if (activity is HasAndroidInjector){
                AndroidInjection.inject(activity)
            }
            (activity as FragmentActivity).supportFragmentManager.registerFragmentLifecycleCallbacks(
                    object  : FragmentManager.FragmentLifecycleCallbacks() {
                        override fun onFragmentCreated(
                            fm: FragmentManager,
                            f: Fragment,
                            savedInstanceState: Bundle?
                        ) {
                            if (f is injectable){
                                AndroidSupportInjection.inject(f)
                            }
                        }
                    },true)
        }
    }
}