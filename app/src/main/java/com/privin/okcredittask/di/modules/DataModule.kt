package com.privin.okcredittask.di.modules

import com.privin.data.ArticleDataRepository
import com.privin.domain.ArticleRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class DataModule {
    @Binds
    abstract fun bindsArticleRepository(articleDataRepository: ArticleDataRepository) : ArticleRepository
}