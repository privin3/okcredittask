package com.privin.okcredittask.di.modules

import com.privin.domain.executor.PostExecutionThread
import com.privin.okcredittask.util.UiThread
import com.privin.okcredittask.view.MainActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UiModule {
    @Binds
    abstract fun bindsPostExecutionThread(uiThread: UiThread) : PostExecutionThread

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeMainActivity() : MainActivity
}