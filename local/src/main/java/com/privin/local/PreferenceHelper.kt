package com.privin.local

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class PreferenceHelper @Inject constructor(context: Context){
    companion object {
        private val PREF_ARTICLE_PACKAGE_NAME = "com.privin.local.preferences"

        private val PREF_KEY_LAST_CACHE = "last_cache"
    }
    private val articlePref : SharedPreferences

    init {
        articlePref = context.getSharedPreferences(PREF_ARTICLE_PACKAGE_NAME,Context.MODE_PRIVATE)
    }

    var lastCacheTime : Long
    get() = articlePref.getLong(PREF_KEY_LAST_CACHE,0)
    set(value) = articlePref.edit().putLong(PREF_KEY_LAST_CACHE,value).apply()
}