package com.privin.local.dao

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.privin.data.entity.MultiMediaEntity
import java.lang.reflect.Type
import java.util.*
import javax.inject.Inject

class MultiMediaTypeConverter{
    val gson = Gson()
    @TypeConverter
    fun objectToString(list: List<MultiMediaEntity>?): String? {
        return gson.toJson(list)
    }

    @TypeConverter
    fun stringToObject(string: String?): List<MultiMediaEntity> {
        string?.let {
            val type = object : TypeToken<List<MultiMediaEntity>>() {}.type
            return gson.fromJson(string, type)
        }
        return Collections.emptyList()
    }
}