package com.privin.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.privin.local.database.Constants
import com.privin.local.model.ArticleDbModel

@Dao
abstract class ArticleDao {
    @Query(Constants.QUERY_ARTICLE+ "WHERE pageSection = :section")
    abstract fun getArticles(section :String): List<ArticleDbModel>

    @Query(Constants.COUNT_ARTICLE +"WHERE pageSection = :section")
    abstract fun isCached(section: String) : Int

    @Query(Constants.DELETE_ALL_ARTICLE+ "WHERE pageSection = :section")
    abstract fun clearArticles(section :String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertArticle(article: ArticleDbModel)
}