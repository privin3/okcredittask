package com.privin.local.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.privin.data.entity.MultiMediaEntity
import com.privin.local.dao.MultiMediaTypeConverter
import com.privin.local.database.Constants

@Entity(tableName = Constants.ARTICLE_TABLE)
data class ArticleDbModel(
    @PrimaryKey(autoGenerate = true)
    var id : Long = 0,
    var title : String?="",
    var abstract : String?="",
    var author : String?="",
    var type : String?="",
    var url : String?="",
    var section : String?="",
    var publishedDate : String?="",
    var pageSection : String?="",
    var multiMediaList : List<MultiMediaEntity>? = ArrayList()) {
}