package com.privin.local.database

object Constants{
    const val ARTICLE_DB= "article_db"

    const val ARTICLE_TABLE = "article"

    private const val SELECT_ALL = "SELECT * FROM"

    const val QUERY_ARTICLE = "$SELECT_ALL $ARTICLE_TABLE "
    const val DELETE_ALL_ARTICLE = "DELETE FROM $ARTICLE_TABLE "

    const val COUNT_ARTICLE = "SELECT COUNT(title) FROM $ARTICLE_TABLE "
}