package com.privin.local.database

import android.content.Context
import androidx.room.*
import com.privin.local.dao.ArticleDao
import com.privin.local.dao.MultiMediaTypeConverter
import com.privin.local.model.ArticleDbModel
import javax.inject.Inject

@Database(entities = [ArticleDbModel::class],version = 1)
@TypeConverters(MultiMediaTypeConverter::class)
abstract class ArticleDatabase @Inject constructor(): RoomDatabase() {



    abstract fun getLocalArticle() : ArticleDao

    companion object {
        private var self : ArticleDatabase? = null
        private val lock = Any()
        fun getInstance(context: Context): ArticleDatabase {
            if (self == null) {
                synchronized(lock) {
                    if (self == null) {
                        self = Room.databaseBuilder(
                            context.applicationContext,
                            ArticleDatabase::class.java,
                            Constants.ARTICLE_DB
                        )
                            .build()
                    }
                }
            }
            return self!!
        }

    }
}