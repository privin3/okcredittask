package com.privin.local

import com.privin.data.entity.ArticleEntity
import com.privin.data.repository.ArticleCache
import com.privin.local.database.ArticleDatabase
import com.privin.local.mapper.ArticleLocalMapper
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class ArticleLocalRepo @Inject constructor(val articleDatabase: ArticleDatabase,
                                           val mapper : ArticleLocalMapper,
                                           val preferenceHelper: PreferenceHelper) : ArticleCache{

    private val EXPIRATION_TIME = (60 * 10 * 1000).toLong()

    override fun clearArticles(section: String): Completable {
        return Completable.defer {
            articleDatabase.getLocalArticle().clearArticles(section)
            Completable.complete()
        }
    }

    override fun saveArticles(articles: List<ArticleEntity>,section : String): Completable {
        return Completable.defer {
            articles.forEach {article ->
                articleDatabase.getLocalArticle().insertArticle(mapper.mapToLocal(article,section))
            }
            Completable.complete()
        }
    }

    override fun getArticles(section: String): Flowable<List<ArticleEntity>> {
        return Flowable.defer {
            Flowable.just(articleDatabase.getLocalArticle().getArticles(section))
        }.map { articles -> articles.map { mapper.mapFromLocal(it) } }
    }

    override fun isCached(section: String): Single<Boolean> {
        return Single.defer {
            Single.just(articleDatabase.getLocalArticle().isCached(section)>0)
        }
    }

    override fun setLastCacheTime(lastCache: Long) {
        preferenceHelper.lastCacheTime = lastCache
    }

    override fun isExpired(): Boolean {
        val currentTime = System.currentTimeMillis()
        val lastUpdateTime = preferenceHelper.lastCacheTime
        return currentTime - lastUpdateTime > EXPIRATION_TIME    }

}