package com.privin.local.mapper

interface LocalMapper<T,V> {
    fun mapFromLocal(type: T): V

    fun mapToLocal(type: V,section : String): T
}