package com.privin.local.mapper

import com.privin.data.entity.ArticleEntity
import com.privin.data.entity.MultiMediaEntity
import com.privin.local.model.ArticleDbModel
import javax.inject.Inject

class ArticleLocalMapper @Inject constructor(): LocalMapper<ArticleDbModel,ArticleEntity> {
    override fun mapFromLocal(type: ArticleDbModel): ArticleEntity {
        return ArticleEntity(title = type.title,
            author = type.author,
            section = type.section,
            abstract = type.abstract,
            type = type.type,
            url = type.url,
            publishedDate = type.publishedDate,
            multiMedia = type.multiMediaList
        )
    }

    override fun mapToLocal(type: ArticleEntity,section: String): ArticleDbModel {
        return ArticleDbModel(title = type.title,
            author = type.author,
            abstract = type.abstract,
            section = type.section,
            type = type.type,
            url = type.url,
            pageSection = section,
            publishedDate = type.publishedDate,
            multiMediaList = type.multiMedia
            )
    }
}